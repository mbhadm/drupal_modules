//Making sure that users are getting a popup windows when pressing sharing buttons.
jQuery(document).ready(function($){
$('.social_media_button_link').click(function(){
   var whatToLink = $(this).attr('href'); 
   var w = 800, h = 800,
   left = Number((screen.width/2)-(w/2)), tops = Number((screen.height/2)-(h/2));
   window.open(whatToLink, 'newwindow', 'width=800 height=600 top='+tops+' left='+left+'');
   return false;
});
});