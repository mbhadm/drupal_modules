<?php

class SocialMediaButtons
{
    private $url;
    private $pageTitle;
    private $path;
    
    function __construct() {
        $this->url = $GLOBALS['base_url'].request_uri();
        $this->pageTitle = drupal_get_title();
        $this->path = drupal_get_path('module', 'social_media_buttons');
        drupal_add_js($this->path . '/js/trigger.js');
    }
    
    public function Buttons($button_to_return) {
        if($button_to_return == 'Facebook') {
            $button_values = array();
            $button_values['button'] = '<a class="social_media_button_link" href="http://www.facebook.com/sharer/sharer.php?u='.$this->url.'&title='.$this->pageTitle.'"><img src="'.$this->path.'/img/facebook.png" /></a>';           
            
            return $button_values;
        }
        
        if($button_to_return == 'Twitter') {
            $button_values = array();
            $button_values['button'] = '<a class="social_media_button_link" href="http://twitter.com/intent/tweet?status='.$this->pageTitle.'+'.$this->url.'"><img src="'.$this->path.'/img/twitter.png" /></a>';

            return $button_values;
        }
        
        if($button_to_return == 'LinkedIn') {
            $button_values = array();
            $button_values['button'] = '<a class="social_media_button_link" href="http://www.linkedin.com/shareArticle?mini=true&url='.$this->url.'&title='.$this->pageTitle.'&source='.$this->pageTitle.'.dk"><img src="'.$this->path.'/img/linkedin.jpg" /></a>';

            return $button_values;
        }
    }
}

